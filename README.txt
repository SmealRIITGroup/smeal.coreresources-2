Introduction
============

The product will add dropdown menu functionality, the PSU search and basic Smeal specific footer information to a Plone site on install. This product is not intended to have much CSS styling of the site itself, with the exception of basic dropdown menu styling to obtain sensible functionality.

Dropdown Menu Usage
===================

The premise behind this navigation tool is to be able to tag items for dropdown display and still maintain Plone's navigation structure in the left hand nav bars.  If you would use Plones "hide from navigation", for example, to hide something from the dropdown, it would also hide it from any left hand navigation bars as well.  This is not desired, as we really should keep our dropdowns short, but normal navigation in the left side be full.

To use dropdown menus on a site, do the following.
1. Install Smeal Core Resources 2.0
2. Create a top level folder for a site so that it shows up in the top navigation bar.  
3. Create any content you want inside this folder.
4. Tag the content you would like to show up in the dropdown as "show_dropdown"
	This will place the item into the dropdown for that folder.
5. You can then tag/untag content to display it in the dropdowns.
