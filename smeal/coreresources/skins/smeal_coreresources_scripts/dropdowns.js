jQuery(function($){
	
	$(document).ready(function(){
		$('div,span,p').removeClass('no-js');
		/* Added this to js to keep drop down visible when tabbing through links
		 * run blur first to make the menu go away when we move to the next tab 
		 * be a bit obsessive with the css and make the focus behave like the hover
		 * */
		$('ul.dropdown li a').blur(function () {
			$(this).parents('.dropdown').addClass('hidden').removeClass('visible');
			$(this).parent().addClass('blur-style').removeClass('focus-style');
	    });
		$('#global-dropdowns li a').blur(function () {
			$(this).siblings('.dropdown').addClass('hidden').removeClass('visible');
		});	
		$('#global-dropdowns li a').focus(function () {
			$(this).siblings('.dropdown').addClass('visible').removeClass('hidden');
		});	
		$('ul.dropdown li a').focus(function () {
			$(this).parents('.dropdown').addClass('visible').removeClass('hidden');
			$(this).parent().addClass('focus-style').removeClass('blur-style');
	    });
		//do this so the green border on top of the item behaves
		// (mostly) the same way on focus as it does on hover
		$('a.main-tab').focus(function () {
			$(this).parent().addClass('has-focus').removeClass('no-focus');
		});	
		$('a.main-tab').blur(function () {
			$(this).parent().addClass('no-focus').removeClass('has-focus');
		});	
		
		
		
			
	}); // end ready
	
	
	
});