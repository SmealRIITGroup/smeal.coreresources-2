"""
This processes which Search Engine to use.
"""

from  Products.PythonScripts.standard  import url_quote

if context.REQUEST['searchSource'] == 'psu':
	    return context.REQUEST.RESPONSE.redirect('http://search-results.aset.psu.edu/search?q=%s&btnG=Search+Penn+State&client=PennState&proxystylesheet=PennState&output=xml_no_dtd&site=PennState' % url_quote(context.REQUEST['SearchableText']))
        
if context.REQUEST['searchSource'] == 'PSUpeople':
	    return context.REQUEST.RESPONSE.redirect('http://www.psu.edu/cgi-bin/ldap/ldap_query.cgi?cn=%s' % url_quote(context.REQUEST['SearchableText']))
        
if context.REQUEST['searchSource'] == 'PSUemail':
	    return context.REQUEST.RESPONSE.redirect('http://www.psu.edu/cgi-bin/ldap/ldap_query.cgi?uid=%s' % url_quote(context.REQUEST['SearchableText']))
        
if context.REQUEST['searchSource'] == 'PSUdept':
	    return context.REQUEST.RESPONSE.redirect('http://www.psu.edu/cgi-bin/ldap/dept_query.cgi?dept_name=%s' % url_quote(context.REQUEST['SearchableText']))
        

else:
	    return context.REQUEST.RESPONSE.redirect('search?SearchableText=%s' % (url_quote(context.REQUEST['SearchableText'])))