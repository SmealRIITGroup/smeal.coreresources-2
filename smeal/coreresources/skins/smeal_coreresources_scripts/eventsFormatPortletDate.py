"""
Formats the Event dates for the portlet.
"""

from DateTime import DateTime

startDate = context.start
endDate = context.end


if DateTime.year(startDate) != DateTime.year(endDate):
   results = [DateTime.Month(startDate), DateTime.dd(startDate) + ',', str(DateTime.year(startDate)), '-', DateTime.Month(endDate), DateTime.dd(endDate) + ',', str(DateTime.year(endDate))]
else:
   if DateTime.Month(startDate) != DateTime.Month(endDate):
      if DateTime.isCurrentYear(startDate):
         results = [DateTime.Month(startDate), DateTime.dd(startDate), '-', DateTime.Month(endDate), DateTime.dd(endDate)]
      else:
         results = [DateTime.Month(startDate), DateTime.dd(startDate), '-', DateTime.Month(endDate), DateTime.dd(endDate) + ',', str(DateTime.year(endDate))]
   else:
      if DateTime.dd(startDate) != DateTime.dd(endDate):
         if DateTime.isCurrentYear(startDate):
            results = [DateTime.Month(startDate), DateTime.dd(startDate), '-', DateTime.dd(endDate)]
         else:
            results = [DateTime.Month(startDate), DateTime.dd(startDate), '-', DateTime.dd(endDate) + ',', str(DateTime.year(startDate))]
      else:
         if DateTime.isCurrentYear(startDate):
            results = [DateTime.Month(startDate), DateTime.dd(startDate)]
         else:
            results = [DateTime.Month(startDate), DateTime.dd(startDate) + ',', str(DateTime.year(startDate))]

return ' '.join(results)
