#INSTRUCTIONS: give this script an id of "getCatInfo"
# then type "/getCatInfo" at the end of any URL for an object in your portal
# to get a printout of the Catalog index values and metadata for that object

from string import join
obid = context.id
path = join(context.getPhysicalPath(), '/')
results = container.portal_catalog(path=path,getId=obid,sort_on='created',sort_order='reverse')
rid = ''
for r in results:
    rid = r.getRID()
if rid:
    print container.portal_catalog.manage_objectInformation(rid=rid)
else:
    print "Not in the catalog, apparently."
return printed
